~programmation ~"semestre 3"

# Programmation modulaire et résiliente
## Descriptif
TODO
## Ressources


Supports :
- https://mindsized.org/s/prez/cours-pmr-2022.html

Ouvrages :
- Functional programming in Scala
- les documentations officielles de [https://www.scala-lang.org/](Scala) et [https://akka.io/docs/](Akka)
- voir également la [webographie](https://mindsized.org/s/prez/cours-pmr-2022.html#webographie)

Outils :
- Installation complète avec notamment ammonite le REPL Scala de Li Haoyi [one click install Scala](https://github.com/scala/scala-lang/blob/main/_posts/2020-06-29-one-click-install.md)
- vscode ou Emacs + Metals

Dépôts git et gitlab :
- snippets : https://gitlab.sorbonne-paris-nord.fr/boudes/cours-pmr-2022
- projet anagrammes : https://gitlab.sorbonne-paris-nord.fr/boudes/anagrams_2022
- partie sur Akka : https://gitlab.sorbonne-paris-nord.fr/boudes/akkatypedfromthedoc
- Pour le backend du projet (Scala, Akka, Akka-http, OpenApi3.0) https://gitlab.sorbonne-paris-nord.fr/boudes/tuto-akka-http-openapi
